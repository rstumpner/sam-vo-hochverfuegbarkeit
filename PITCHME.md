## Systemadministration
#### Vorlesung

---
## Verfügbarkeit
(Begriffserklärung)

Die Verfügbarkeit eines technischen Systems Beschreibt einen Zeitrahmen in dem dieses System die Definierten Anforderungen erfüllt.


---
## Verfügbarkeit
(Berechnung)

Verfügbarkeit in % = (Betriebszeit - Ausfallszeit / Betriebszeit) * 100

---
## Verfügbarkeit
(Beispiel)

Verfügbarkeit in 99,7253% = ((8736-24)÷(24×7×52))×100

---
## Verfügbarkeitsklassen
#### Availability Environment Classification

Definiert von der Harward Research Group (https://hrgresearch.com/)

---
## Verfügbarkeit Klasse 1

* AEC – 0
* Conventional
* Funktion kann Unterbrochen werden
* Datenintegrität ist nicht Essenziell
* Verfügbarkeit ist kleiner 99% im Jahr
* Erlaubte Zeit des Ausfalls größer 87,6 Stunden in einem Jahr

---
## Verfügbarkeit Klasse 2

* AEC-1
* Highly Reliable
* Funktion kann unterbrochen werden,
* Datenintegrität muss gewährleistet sein
* Verfügbarkeit ist 99% im Jahr
* Erlaubte Zeit des Ausfalls 87,6 Stunden in einem Jahr

---
## Verfügbarkeit Klasse 2
#### Beispiel

![DC Tier 2](_images/verfuegbarkeit-example-dc.png)

---
## Verfügbarkeit Klasse 3

* AEC – 2
* High Availability
* Funktion darf nur innerhalb festgelegter Zeiten oder zur Hauptbetriebszeit minimal unterbrochen werden
* Verfügbarkeit ist 99,9% im Jahr
* Erlaubte Zeit des Ausfalls 8:45:58 Stunden im Jahr

---
## Verfügbarkeit Klasse 4

* AEC – 3
* Fault Resilient
* Funktion muss innerhalb festgelegter Zeiten oder während der Hauptbetriebszeit ununterbrochen aufrechterhalten werden
* Verfügbarkeit ist 99,99% im Jahr
* Erlaubte Zeit des Ausfalls ( 52:36 Minuten im Jahr)

---
## Verfügbarkeit Klasse 5

* AEC – 4
* Fault Tolerant
* Funktion muss ununterbrochen aufrechterhalten werden, 24/7-Betrieb muss gewährleistet sein
* Verfügbarkeit ist 99,999% im Jahr
* Erlaubte Zeit des Ausfalls 5:16 Minuten im Jahr

---
## Verfügbarkeit Klasse 6

* AEC – 5
* Disaster Tolerant
* Funktion muss unter allen Umständen verfügbar sein
* Verfügbarkeit ist  99,9999% im Jahr
* Erlaubte Zeit des Ausfalls  31,6 Sekunden im Jahr

---
## Verfügbarkeit
(Tabelle)

|Verfügbarkeitsklasse | Verfügbarkeit | erlaubte Ausfallzeit |
|---|---|---|
| Klasse 1 (AEC-0) | < 99 % | < 87,6 Stunden |
| Klasse 2 (AEC-1) | 99   % |   87,6 Stunden |
| Klasse 3 (AEC-2) | 99,9 % | 8:45:58 Stunden |
| Klasse 4 (AEC-3) | 99,99 % | 52,6 Minuten |
| Klasse 5 (AEC-4) |   99,999 % | < 5,16 Minuten |
| Klasse 6 (AEC-5) |   99,9999 % | < 31,6 Sekunden |

---
## Summary

Wichtig ist bei der Betrachtung eines Gesamtsystems (zb. Rechenzentrum) wird die Verfügbarkeit am Service gemessen müssen die unteren Schichten (Hardware / Infrastruktur ) beachtet werden

---
## Verfügbarkeit

![Verfügbarkeit Skizze](_images/verfuegbarkeit-skizze.png)

---
## Wartungsfenster
* Wartungsfenster werden in der IT geplante Wartungen mit Beeinträchtigung oder Ausfall der Betroffenen Systeme genannt
* Werden oft bei der Verfügbarkeitsberechnung von SLAs nicht berücksichtigt

---
## Service Level Agreement
#### Bezeichnet eine Vertraglich zugesicherte Verfügbarkeit oder Qualität eines Angebotenen Services

---
## SLA Indikatoren
* Betriebszeiten
* Verfügbarkeit
* Wartungszeiten
* Konventionalstrafen

---
## Mean Time between Failure (MTBF)

Beschreibt die Durchschnittliche Zeit bis mit einer sehr hohen Wahrscheinlichkeit wieder ein Fehler in diesem System Auftritt

---
## Mean Time to Repair (MTTR)

Beschreibt die Dauer vom Ausfall des Services , bis zur Vollständigen
Wiederherstellung dieses Service

---
## Clustersysteme
### Ist ein Verbund aus Rechnern um die Verfügbarkeit vom Gesamtsystem (Dienst) zu erhöhen
* Frei von Single Point of Failures

---
## Clustersysteme
(Arten)

* Shared-all
* Shared-nothing
* Aktiv / Standby


---
## Active / Standby (Asyncron)
### Bezeichnet ein System bei dem eine Komponente Aktiv läuft und die zweite im Fehlerfall Aktiviert wird
* Die zweite Komponente/Dienste muss im Fehlerfall erst gestartet werden
* Disk / VM

---
## Active / Active
### In einem Active / Active System stehen auf allen Nodes alle Dienste immer zur Verfügung im Fehlerfall wird auf das zweite System umgeschaltet
* Die zweite Komponente wird im Fehlerfall nur in den Aktiven Zustand versetzt

* Disk / VM-FT
* Statefull Firewall / Switches (VSS / Stacking) / Storage

---
## Shared All (Statefull)
### Bei einem Shared all Cluster System werden alle Dienste und alle Daten Syncron gehalten

* Über die Direkte Clusterverbindung wird meist auch der Hauptspeicher des Primärsystems kopiert
* Disk / VM-FT
* Statefull Firewall / Switches (VSS / Stacking) / Storage

---
## Shared All
Beispiel

![Skizze Shared all](_images/cluster-shared-memory.png)

---
## Shared Nothing (Stateless)
### Bei einem Shared Nothing Cluster System wird der Service (Webserver / VM / Loadbalancer) auf dem Sekundärsystem gestartet

* Vmware Cluster / Pacemaker


---
## Loadbalancing Cluster
### Um nicht nur die Verfügbarkeit zu erhöhen sondern auch die Leistungsfähigkeit wird bei größeren Systemen ein Loadbalancer Clustering Verwendet
* Die Knoten des Systems sind Stateless und mehrfach Vorhanden
* Die Lastaufteilung kann durch DNS , Anycast oder Loadbalancer Systeme
erreicht werden
* NO-SQL Server-Cluster
* Webapplikationen

---
## HPC-Cluster (DCOS)
### Kommt aus dem High Performance Computing Umfeld und dient zur Berchnung Komplexer Aufgaben
* Eine große Aufgabe wird in kleiner Aufgaben geteilt (Job Management System) und an die Cluster Nodes (Worker) Verteilt
* Vermehrter Einsatz bei BigData ( Hadoop)
* Arbeitsweise für Container Hochverfügbarkeit (Kubernetes / Mesos / Marathon)

---
## Single Point of Failure
* Wie schon beschrieben werden viele Systeme Hochverfügbar gemacht in dem Redundante Systeme geschaffen werden
* Bei diesem Konzept hat die Vermeidung von sogenannten Single Point of
Failures bei der Betrachtung des Gesamtsystems oberstes Gebot

---
## Datensicherheit (Verfügbarkeit)
* Das Bedürfnis auch im Fehlerfall noch auf die Daten zugreifen zu können oder diese Wiederherstellen zu können

* Disaster Recovery
* MTTR (Stunden / Tage)

---
## Snapshots
### Bei einem Snapshot wird zu einem gewissen Zeitpunkt eine Momentaufnahme von Daten oder eines Systems erzeugt
* Copy on Write
* Redirect on Write
* Blocklevel Ebene (Storage / LVM)
* Dateisystem Ebene ( ZFS / BTRFS / VSS)

---
## Snapshots Redirect on Write
* Alle neuen Daten werden in den Snapshot Umgeleitet
* Beim Auflösen vom Snapshot wird Platz benötigt um wieder ein Aktuelles
System zu erhalten (Konsolitierung)
* Hypervisor (neues Disk Image)

---
## Snapshots Copy on Write
* Alle alten Daten werden als Version Abgelegt
* Benötigt keine Zusätzlichen Resourcen
* Löschen von alten Versionen ist einfach
* Copy on Write Dateisysteme (ZFS / BTRFS)
* LVM2

---
## Replikation
* Ist eine entfernte Kopie der Daten oder eines Systems
* Erhöhung der Datensicherheit
* Lastverteilung bei Read-Only Kopien
* Syncrone Replikation
* Asyncrone Replikation
#### Beispiel:
* Storage / Blocklevel / Filesystem / VM

---
## Syncrone Replikation
### Um eine syncrone Replikation gewährleisten zu können gilt die Veränderung erst dann als Erfolgreich wenn beide Systeme die Daten geschrieben haben
* Atomares (Unteilbarkeit) der Transaktion Commit-Protokoll
* Datenintegrität is gegeben
* Primäres und Sekundäres System können sich ausbremsen
* Secoundarys können als Read-Only verwendet werden
* SQL Server
* Storages / DRBD

---
## Asyncrone Replikation
* Bei dieser werden die Daten Zeitverzögert an das Zielsystem geschrieben
* Am Primären System wird kein Commit Abgewartet
* Datenintegrität ist nicht per se gegeben
* Merge Replikation
* Snapshot Replikation

---
## Backup
### Bei einem Backup werden periodisch Kopieen eines Systems Erzeugt um im Fehlerfalle durch einen Restore Prozess die Daten oder das System Wiederherstellen zu können
* Vollsicherung
* Differnzielle Sicherung
* Inkrementell Sicherung
* Komprimierung / Deduplizierung / Verschlüsselung von Backups

---
## Backup Vollsicherung
### Bei der Vollsicherung werden die zu sichernden Daten Vollstänig auf das Backupmedim Übertragen und kann ohne weiteres Zurückgesichert weden
* Benötigt viel Platz
* Zeitaufwendig
* Einfache Implementierung in der Backupsoftware möglich

---
## Backup Differenzielle Sicherung
* Speichert die Veränderungen gegenüber der letzten Vollsicherung
* Somit wird immer die Vollsicherung und die Differenzielle Sicherung benötigt
* Platzsparender als ein Vollbackup
* Zeitersparnis gegenüber dem Vollbackup

---
## Backup Inkrementelle Sicherung
### Bei der Inkrementellen Sicherung werden immer nur die Veränderten Daten zu letzten Inkrementellen Sicherung (bei der ersten zur Komplettsicherung) berücksichtigt
* Es wird die Vollsicherung und alle Inkrementellen Sicherungen benötigt
* Löschen von Backupsätzen ist sehr Aufwendig
* Weniger Platzbedarf
* Schnelle Sicherung möglich

---
## Backup Speicherabbild (Image)
### Bei der Speicherabblidsicherung werden nich nur die Notwendigen Programmdaten gesichert , sondern eine 1 zu 1 Kopie der Systeme
* Komplexer Restore bei Bare Metal Systemen
* VM / Container und Co
* Unterstützung des Hypervisors oder der Containersoftware
* Granularer Restore von einzelnen Dateien möglich (Atomar Restore)

---
## Betriebssicherheit (Availability)
### Maßnahmen die Ergriffen werden können um im Fehlerfall die Verfügbarkeit des Systems zu gewährleisten oder zu Verhindern

* MTTR (Sekunden)
* MTBF (Jahre)
* Postmortem (Training)
* Nofallplan
* KISS
* Faktor Mensch
